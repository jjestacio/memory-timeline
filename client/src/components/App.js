import React, { Component } from 'react'

export default class App extends Component {
	constructor() {
		super();

		this.state = {
			counter: 0,
		};

		this.convertToTime = this.convertToTime.bind(this);
	}

	componentDidMount() {
		setInterval(() => this.setState({ counter: this.state.counter + 1}), 1000);
	}

	convertToTime(totalTimeSeconds) {
		let seconds = Math.floor(totalTimeSeconds) % 60;
		let minutes = Math.floor(totalTimeSeconds / 60);
		const hours = Math.floor(minutes / 60);

		if (minutes < 10)
			minutes = "0" + minutes;
		if (seconds < 10)
			seconds = "0" + seconds;

		return hours + ":" + minutes + ":" + seconds;
	}

	render() {
		const { counter } = this.state;

		return (
			<div className="page">
				<span>Time Elapsed: { this.convertToTime(counter) }</span>
			</div>
		);
	}
}
